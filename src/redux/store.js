import {configureStore} from '@reduxjs/toolkit';
import authReducer from '../features/auth/auth.slice';
import cradleReducer from '../features/cradle/cradle.slice';
import devicesReducer from '../features/devices/devices.slice';
export const store = configureStore({
  reducer: {
    auth: authReducer,
    cradle: cradleReducer,
    devices: devicesReducer,
  },
});
