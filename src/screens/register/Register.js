import {
  Text,
  View,
  StyleSheet,
  Image,
  ImageBackground,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {React} from 'react-native';
import {images, icons} from '../../constants/index';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useState} from 'react';
import {
  isValidEmail,
  isValidPassword,
  isValidUser,
} from '../../utilies/Validations';
import {fetchSignUp} from '../../features/auth/auth.slice';
import {useDispatch} from 'react-redux';

function Register({navigation}) {
  const dispatch = useDispatch();
  //State for validating:
  const [errorEmail, setErrorEmail] = useState('');
  const [errorPassword, setErrorPassword] = useState('');
  const [errorUser, setErrorUser] = useState('');
  const [errConfirmPassword, seterrConfirmPassword] = useState('');
  //State to store email/password
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [user, setUser] = useState('');
  const [confirmPassword, setconfirmPassword] = useState('');

  const handlePressRegister = () => {
    console.log('email: ', email, ' pass: ', password);
    dispatch(
      fetchSignUp({
        name: user,
        email: email,
        password: password,
        confirmPassword: confirmPassword,
      }),
    );
    navigation.goBack();
  };
  return (
    <View style={styles.container}>
      <ImageBackground
        source={images.background}
        resizeMode="cover"
        style={styles.container}>
        <View>
          <View style={styles.header}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack();
              }}>
              <Image source={icons.icon_back} style={styles.icon_back} />
            </TouchableOpacity>
            <Image source={icons.icon_cradle} style={styles.icon_cradle} />
            <Text style={styles.textHeader}>Smart Cradle App</Text>
            <View style={styles.container}></View>
            <Image source={icons.icon_question} style={styles.icon_question} />
          </View>
        </View>

        <View style={styles.titleHeader}>
          <Text style={styles.titleRegister}>REGISTER</Text>
          <Text style={styles.titleTextRegister}>
            {' '}
            Please input your information
          </Text>
        </View>

        <View style={styles.input}>
          <Text style={styles.textTittleInput}>Username: </Text>

          <View style={{flexDirection: 'row'}}>
            <TextInput
              style={styles.textInput}
              placeholder="dinomanh_2k1"
              showSoftInputOnFocus={false}
              onChangeText={text => {
                setErrorUser(
                  isValidUser(text) === true
                    ? ' '
                    : 'User is not in correct form',
                );
                setUser(text);
              }}
              value={user}
              placeholderTextColor={'rgba(0,0,0,0.6)'}
            />
            <Icon
              name="user-circle-o"
              size={25}
              color="rgba(0,0,0,0.6)"
              style={styles.iconInput}
            />
          </View>
          <Text
            style={{
              color: '#DC143C',
              fontSize: 13,
              marginLeft: 10,
              marginTop: 5,
              fontWeight: 'bold',
            }}>
            {errorUser}
          </Text>
          <Text style={styles.textTittleInput}>Email: </Text>
          <View style={{flexDirection: 'row'}}>
            <TextInput
              style={styles.textInput}
              placeholder="Example123@gmail.com"
              showSoftInputOnFocus={false}
              onChangeText={text => {
                setErrorEmail(
                  isValidEmail(text) === true
                    ? ' '
                    : 'Email is not in correct form',
                );
                setEmail(text);
              }}
              value={email}
              placeholderTextColor={'rgba(0,0,0,0.6)'}></TextInput>
            <Icon
              name="envelope"
              size={25}
              color="rgba(0,0,0,0.6)"
              style={styles.iconInput}
            />
          </View>
          <Text
            style={{
              color: '#DC143C',
              fontSize: 13,
              marginLeft: 10,
              marginTop: 5,
              fontWeight: 'bold',
            }}>
            {errorEmail}
          </Text>

          <Text style={styles.textTittleInput}>Password: </Text>
          <View style={{flexDirection: 'row'}}>
            <TextInput
              style={styles.textInput}
              secureTextEntry={true}
              showSoftInputOnFocus={false}
              onChangeText={text => {
                setErrorPassword(
                  isValidPassword(text) == true
                    ? ' '
                    : 'Password must be at least one number and length at least 8 ',
                );
                setPassword(text);
              }}
              value={password}
              placeholder="Enter your password"
              placeholderTextColor={'rgba(0,0,0,0.6)'}></TextInput>
            <Icon
              name="lock"
              size={30}
              color="rgba(0,0,0,0.6)"
              style={styles.iconInput}
            />
          </View>
          <Text
            style={{
              color: '#DC143C',
              fontSize: 13,
              marginLeft: 10,
              marginTop: 5,
              fontWeight: 'bold',
            }}>
            {errorPassword}
          </Text>
          <Text style={styles.textTittleInput}>Confirm password: </Text>
          <View style={{flexDirection: 'row'}}>
            <TextInput
              style={styles.textInput}
              secureTextEntry={true}
              showSoftInputOnFocus={false}
              onChangeText={text => {
                setconfirmPassword(text);
              }}
              value={confirmPassword}
              placeholder="Enter your confirm password"
              placeholderTextColor={'rgba(0,0,0,0.6)'}></TextInput>
            <Icon
              name="lock"
              size={25}
              color="rgba(0,0,0,0.6)"
              style={styles.iconInput}
            />
          </View>
          <Text
            style={{
              color: '#DC143C',
              fontSize: 13,
              marginLeft: 10,
              marginTop: 5,
              fontWeight: 'bold',
            }}>
            {errConfirmPassword}
          </Text>
        </View>

        <View>
          <TouchableOpacity
            style={{
              borderColor: 'white',
              borderWidth: 1,
              height: 45,
              borderRadius: 5,
              marginHorizontal: 25,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={handlePressRegister}>
            <Text style={styles.TextLogin}>Register</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    height: 40,
  },
  textHeader: {
    marginTop: 6,
    color: 'white',
    fontSize: 22,
    fontWeight: 'bold',
  },
  icon_back: {
    marginHorizontal: 5,
    width: 25,
    height: 25,
    tintColor: 'white',
  },
  icon_cradle: {
    marginHorizontal: 5,
    width: 28,
    height: 28,
    tintColor: 'white',
  },
  icon_question: {
    marginRight: 5,
    width: 26,
    height: 26,
    tintColor: 'white',
  },

  //titleHeader:
  titleHeader: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
  },
  titleRegister: {
    color: 'white',
    fontSize: 26,
    fontWeight: 'bold',
  },
  titleTextRegister: {
    marginVertical: 6,
    color: 'white',
    fontSize: 16,
  },

  //input:
  input: {
    marginVertical: 5,
    marginHorizontal: 15,
  },
  iconInput: {
    position: 'absolute',
    left: 20,
    top: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textTittleInput: {
    color: 'white',
    fontSize: 16,
    marginLeft: 15,
    fontWeight: 'bold',
  },
  textInput: {
    marginTop: 8,
    color: 'black',
    marginLeft: 10,
    borderColor: 'white',
    height: 45,
    marginHorizontal: 15,
    backgroundColor: 'white',
    fontSize: 16,
    flex: 1,
    paddingLeft: 45,
    borderRadius: 30,
  },
  //login:

  TextLogin: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 18,
  },
  iconOtherMethod: {
    marginTop: 10,
    marginHorizontal: 10,
  },
});

export default Register;

