import {useEffect, useState} from 'react';
import {
  Pressable,
  Modal,
  Text,
  View,
  StyleSheet,
  Image,
  ImageBackground,
  TouchableOpacity,
  TextInput,
  Dimensions,
} from 'react-native';
import {React} from 'react-native';
import {images, icons} from '../../constants';
import DetailCradle from './DetailCradle';
import {FlatList} from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  useGetIdCradle,
  useGetCradle,
  useGetDeviceIds,
} from '../../hooks/useGetCradle';
import {useDispatch} from 'react-redux';
import {fetchPostCradleData} from '../../features/cradle/cradle.slice';
import {useGetDevices} from '../../hooks/useGetDevices';
import UITextModalHome from './components/UITextModalHome';
function Home({navigation}) {
  const {width: withScreen} = Dimensions.get('window');
  const dispatch = useDispatch();
  const [modalAllCradle, setModalAllCradle] = useState(false);
  const [data, setdata] = useState([]);
  const [currentIndex, setcurrentIndex] = useState();
  const [refFlatList, setrefFlatList] = useState();
  const [modalCreate, setModalCreate] = useState(false);
  const [currentDeviceIds, setcurrentDeviceIds] = useState([]);
  const [IdCradle] = useGetIdCradle();

  const [Name, setName] = useState();
  const [Description, setDescription] = useState();
  const [Sku, setSku] = useState();
  const [Avatar, setAvatar] = useState();

  const [DataAllCradle] = useGetCradle();

  // DataAllCradle.map(item => {
  //   console.log('item.id', item.deviceIds);
  // });
  //  const dataIdCradle = useGetIdCradleData();
  //console.log('=========== Data for ID Cradle from Home', dataIdCradle);
  const [DataAllDevices] = useGetDevices();
  const [deviceIds] = useGetDeviceIds();
  const getItemLayout = (data, index) => {
    return {length: 340, offset: 340 * index, index};
  };
  useEffect(() => {
    setdata(DataAllCradle.data);
  }, [DataAllCradle.data]);
  useEffect(() => {
    setcurrentDeviceIds(deviceIds);
  }, [deviceIds]);

  const onClickListCradle = (item, index) => {
    // dispatch(fetchIdCradleData(item.id));
    console.log('======== deviceIds from Home', currentDeviceIds);
    setcurrentIndex(index + 1);
    const newCradleData = data.map((e, index) => {
      if (item.id === e.id) {
        return {
          ...e,
          selected: true,
        };
      }
      return {
        ...e,
        selected: false,
      };
    });
    setdata(newCradleData);
  };
  return (
    <View style={{flex: 1}}>
      <ImageBackground style={{flex: 1}} source={images.background}>
        <View
          style={{
            height: 40,
            width: '100%',
            backgroundColor: '#1E90FF',
            justifyContent: 'center',
          }}>
          <View style={styles.header}>
            <TouchableOpacity
              onPress={() => {
                navigation.openDrawer();
              }}>
              <Image source={icons.icon_menu} style={styles.icon_back} />
            </TouchableOpacity>
            <Image source={icons.icon_home} style={styles.icon_home_title} />
            <Text style={styles.textHeader}>HOME</Text>
          </View>
        </View>
        <View
          style={{
            marginVertical: 30,
            flex: 1,
            backgroundColor: 'white',
            borderRadius: 40,
            borderWidth: 2,
            marginHorizontal: 20,
            marginBottom: 25,
          }}>
          <View
            style={{
              height: 80,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: '#1E90FF',
                fontSize: 22,
                fontWeight: 'bold',
              }}>
              {' '}
              Here are your cradle specs:
            </Text>
          </View>
          {data && (
            <View
              style={{
                marginHorizontal: 5,
              }}>
              <FlatList
                showsHorizontalScrollIndicator={false}
                horizontal={true}
                data={data}
                keyExtractor={item => item.id}
                contentContainerStyle={{
                  width: withScreen * data.length,
                  height: 450,
                }}
                getItemLayout={getItemLayout}
                pagingEnabled={true}
                ref={ref => {
                  setrefFlatList(ref);
                }}
                renderItem={({item, index}) => (
                  <DetailCradle
                    keyExtractor={item.id}
                    id={item.id}
                    index={index + 1}
                    name={item.name}
                    description={item.description}
                    status={item.status}
                    devices={item.devices}
                  />
                )}
              />
            </View>
          )}
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalAllCradle}>
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <View>
                  <FlatList
                    data={data}
                    keyExtractor={item => item.id}
                    renderItem={({item, index}) => (
                      <TouchableOpacity
                        //   keyExtractor={item.id}
                        onPress={() => onClickListCradle(item, index)}
                        style={[
                          styles.btnListCradle,
                          {
                            backgroundColor: item.selected
                              ? '#FF9900'
                              : 'white',
                          },
                        ]}>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          <View
                            style={{
                              marginLeft: 3,
                              justifyContent: 'center',
                              alignItems: 'center',
                              backgroundColor: '#53d6f2',
                              width: 40,
                              height: 40,
                              borderRadius: 15,
                            }}>
                            <Text style={styles.TextListCradle}>
                              0{index + 1}
                            </Text>
                          </View>
                          <Text
                            style={{
                              marginLeft: 20,
                              fontWeight: 'bold',
                              fontSize: 18,
                            }}>
                            {item.name}
                          </Text>
                        </View>
                      </TouchableOpacity>
                    )}></FlatList>
                  <TouchableOpacity
                    style={[styles.button, styles.buttonOpen]}
                    onPress={() => {
                      setModalAllCradle(!modalAllCradle);
                      data.map(item => {
                        if (item.selected === true) {
                          item.selected = false;
                        }
                      });
                      // AsyncStorage.getItem('accessToken').then(token => {
                      //   currentDeviceIds.map(item => {
                      //     console.log('======= ID press Open', item);
                      //     dispatch(
                      //       fetchIdDevicesData({
                      //         id: item,
                      //         token: token,
                      //       }),
                      //     );
                      //   });
                      // });
                      refFlatList.scrollToIndex({
                        animated: true,
                        index: currentIndex - 1,
                      });
                    }}>
                    <Text style={styles.textStyle}>Open</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[styles.button, styles.buttonClose]}
                    onPress={() => {
                      setModalAllCradle(!modalAllCradle);
                      data.map(item => {
                        if (item.selected === true) {
                          item.selected = false;
                        }
                      });
                      setModalAllCradle(!modalAllCradle);
                    }}>
                    <Text style={styles.textStyle}>Close</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>

          <Pressable
            style={styles.btnAllCradle}
            onPress={() => {
              setModalAllCradle(true);
            }}>
            <Text style={styles.textStyle}>All Cradle</Text>
          </Pressable>
          <Pressable
            style={styles.btnCreateCradle}
            onPress={() => {
              setModalCreate(true);
            }}>
            <Text style={styles.textStyle}>Create</Text>
          </Pressable>

          <View style={styles.centeredView}>
            <Modal
              animationType="slide"
              transparent={true}
              visible={modalCreate}
              onRequestClose={() => {
                setModalAllCradle(!modalCreate);
              }}>
              <View style={styles.centeredView}>
                <View style={styles.modalView}>
                  <UITextModalHome input="Name:" />
                  <TextInput
                    showSoftInputOnFocus={false}
                    onChangeText={text => setName(text)}
                    defaultValue={Name}
                    style={styles.textInputModal}
                    editable={true}
                  />
                  <UITextModalHome input="Description:" />
                  <TextInput
                    showSoftInputOnFocus={false}
                    onChangeText={text => setDescription(text)}
                    defaultValue={Description}
                    style={styles.textInputModal}
                    editable={true}
                  />
                  <UITextModalHome input="Sku:" />
                  <TextInput
                    showSoftInputOnFocus={false}
                    onChangeText={text => setSku(text)}
                    defaultValue={Sku}
                    style={styles.textInputModal}
                    editable={true}
                  />
                  <UITextModalHome input="Avatar:" />
                  <TextInput
                    showSoftInputOnFocus={false}
                    onChangeText={text => setAvatar(text)}
                    defaultValue={Avatar}
                    style={styles.textInputModal}
                    editable={true}
                  />

                  <Pressable
                    style={[styles.button, styles.buttonClose]}
                    onPress={() => {
                      AsyncStorage.getItem('accessToken').then(token => {
                        console.log('============= name cradle created', Name);
                        dispatch(
                          fetchPostCradleData({
                            token: token,
                            data: {
                              name: Name,
                              description: Description,
                              sku: Sku,
                              avatar: Avatar,
                            },
                          }),
                        );
                      });
                      setName('');
                      setDescription('');
                      setSku('');
                      setModalCreate(!modalCreate);
                    }}>
                    <Text style={styles.textStyle}>Create</Text>
                  </Pressable>
                  <Pressable
                    style={[styles.button, styles.buttonClose]}
                    onPress={() => {
                      setName('');
                      setDescription('');
                      setSku('');
                      setModalCreate(!modalCreate);
                    }}>
                    <Text style={styles.textStyle}>Close</Text>
                  </Pressable>
                </View>
              </View>
            </Modal>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
}
const styles = StyleSheet.create({
  header: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
  },
  textHeader: {
    marginHorizontal: 10,
    color: 'white',
    fontSize: 22,
    fontWeight: 'bold',
  },
  icon_back: {
    marginHorizontal: 5,
    width: 20,
    height: 20,
    tintColor: 'white',
  },
  icon_cradle: {
    marginHorizontal: 5,
    width: 35,
    height: 35,
    tintColor: 'white',
  },
  icon_home: {
    marginHorizontal: 5,
    width: 60,
    height: 60,
    tintColor: '#1E90FF',
  },
  icon_noise: {
    marginHorizontal: 5,
    width: 80,
    height: 80,
    tintColor: '#66CCFF',
  },
  icon_fan_music: {
    marginHorizontal: 5,
    width: 30,
    height: 30,
    tintColor: '#1E90FF',
  },
  icon_home_title: {
    marginLeft: 120,
    width: 25,
    height: 25,
    tintColor: 'white',
  },
  welcomeText: {
    marginTop: 20,
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
  },
  titleText: {
    marginVertical: 10,
    color: 'white',
    fontSize: 30,
    fontWeight: 'bold',
  },
  btnLogin: {
    //marginBottom: 60,
    borderColor: 'white',
    borderWidth: 1,
    height: 45,
    borderRadius: 25,
    marginHorizontal: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1E90FF',
  },
  TextLogin: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
  styleModal: {
    color: 'black',
    backgroundColor: 'red',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 25,
    marginLeft: 10,
  },
  modalView: {
    width: 270,
    // height: 450,
    marginRight: 8,
    marginBottom: 20,
    backgroundColor: '#EEEEEE',
    borderRadius: 20,
    borderWidth: 2,
  },
  button: {
    marginTop: 5,
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#2196F3',
    marginHorizontal: 10,
  },
  buttonClose: {
    backgroundColor: '#2196F3',
    marginHorizontal: 10,
    marginBottom: 5,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  btnListCradle: {
    marginVertical: 4,
    height: 45,
    borderRadius: 28,
    marginHorizontal: 10,
    justifyContent: 'center',
    alignItems: 'flex-start',
    // backgroundColor: 'white',
  },
  TextListCradle: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
  btnAllCradle: {
    marginTop: 5,
    borderColor: 'white',
    borderWidth: 1,
    height: 45,
    borderRadius: 25,
    marginHorizontal: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1E90FF',
  },
  btnCreateCradle: {
    marginTop: 5,
    borderColor: 'white',
    borderWidth: 1,
    height: 45,
    borderRadius: 25,
    marginHorizontal: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1E90FF',
  },

  textInputModal: {
    marginTop: 5,
    color: 'black',
    marginLeft: 10,
    borderColor: 'white',
    height: 45,
    paddingLeft: 22,
    marginHorizontal: 5,
    backgroundColor: 'white',
    fontSize: 18,
    borderRadius: 30,
  },
});

export default Home;
