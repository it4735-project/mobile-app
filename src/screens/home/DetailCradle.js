import {useEffect, useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  Switch,
  TouchableOpacity,
  Modal,
  Pressable,
} from 'react-native';
import {React} from 'react-native';
import {icons} from '../../constants';
import {useDispatch, useSelector} from 'react-redux';
import {useGetCradle} from '../../hooks/useGetCradle';
import {
  fetchDeleteIdCradleData,
  fetchPutIdCradleData,
  fetchRemoveDevice,
} from '../../features/cradle/cradle.slice';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {TextInput} from 'react-native-gesture-handler';
import {
  fetchDeleteIdDevice,
  fetchIdDevicesData,
  fetchPutIdDevice,
} from '../../features/devices/devices.slice';
import UITextModalDetail from './components/UITexModalDetail';

import {useGetIdDevice} from '../../hooks/useGetDevices';

function DetailCradle({id, index, name, description, status, devices}) {
  const dispatch = useDispatch();
  const [isEnabledFan, setEnabledFan] = useState();
  const [isEnabledMusic, setEnabledMusic] = useState();
  const [DataAllCradle] = useGetCradle();

  const DeviceData = useGetIdDevice();
  console.log('Device data from detail cradle', DeviceData);
  console.log('Data all devices', devices[0]);
  const [Name, setName] = useState();
  const [Description, setDescription] = useState();
  const [Status, setStatus] = useState();
  const [editItem, setEditItem] = useState();

  const [Avatar, setAvatar] = useState();
  const [DeviceName, setDeviceName] = useState();
  const [DeviceDes, setDeviceDes] = useState();
  const [DeviceId, setDeviceId] = useState();

  const [modalDeleteCradle, setModalDeleteCradle] = useState(false);
  const [modalPutCradle, setModalPutCradle] = useState(false);
  const [modalDeleteDevice, setModalDeleteDevice] = useState(false);

  const handleChangeFan = () => {
    setEnabledFan(!isEnabledFan);
  };
  const handleChangeMusic = () => {
    setEnabledMusic(!isEnabledMusic);
  };

  return (
    DataAllCradle && (
      <View
        style={{
          width: 300,
          height: 450,
          backgroundColor: 'white',
          borderRadius: 40,
          borderWidth: 4,
          marginHorizontal: 20,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              flexDirection: 'row',
            }}>
            <Image
              source={icons.icon_active}
              style={{
                right: 0,
                marginTop: 5,
                position: 'absolute',
                width: 40,
                height: 40,
                marginRight: 88,
                tintColor: status === 'active' ? '#66FF00' : 'red',
              }}></Image>
            <TouchableOpacity
              onPress={() => {
                setModalPutCradle(true);
                setName(name);
                setDescription(description);
                setStatus(status);
                setEditItem(id);
              }}>
              <Text
                style={{
                  marginTop: 6,
                  color: status === 'active' ? '#1E90FF' : 'grey',
                  fontSize: 28,
                  fontWeight: 'bold',
                }}>
                {name}
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              right: 0,
              flexDirection: 'row',
              position: 'absolute',
            }}>
            <TouchableOpacity
              onPress={() => {
                console.log('======= Press X');
                setModalDeleteCradle(true);
              }}>
              <Image
                source={icons.icon_x}
                style={{
                  marginTop: 13,
                  width: 20,
                  height: 20,
                  marginRight: 10,
                  tintColor: '#1E90FF',
                }}></Image>
            </TouchableOpacity>
          </View>
        </View>

        <View
          style={{
            flexDirection: 'row',
            marginTop: 20,
          }}>
          <TouchableOpacity
            style={{
              marginLeft: 45,
              height: 100,
              width: 100,
            }}
            onPress={() => {
              console.log('id', devices[0].id);
              AsyncStorage.getItem('accessToken').then(token => {
                dispatch(
                  fetchIdDevicesData({
                    id: devices[0].id,
                    token: token,
                  }),
                );
              });
              setDeviceId(devices[0].id);
              setModalDeleteDevice(true);
            }}>
            <Image
              source={icons.icon_cradle_home}
              style={[
                styles.icon_home,
                {
                  width: 60,
                  height: 60,
                  tintColor: devices[0].deviceType === 1 ? '#1E90FF' : 'grey',
                },
              ]}></Image>
            <Text
              style={{
                marginLeft: 18,
                fontSize: 17,
                fontWeight: 'bold',
              }}>
              {/* {temp} */}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              marginLeft: 25,
            }}
            onPress={() => {
              AsyncStorage.getItem('accessToken').then(token => {
                dispatch(
                  fetchIdDevicesData({
                    id: devices[1].id,
                    token: token,
                  }),
                );
              });
              setDeviceId(devices[1].id);
              setModalDeleteDevice(true);
              //  setModalDeleteDevice(true);
            }}>
            <Image
              source={icons.icon_temperature}
              style={[
                styles.icon_home,
                {
                  width: 55,
                  height: 55,
                  tintColor: devices[1].deviceType === 2 ? '#1E90FF' : 'grey',
                },
              ]}></Image>
            <Text
              style={{
                marginLeft: 22,
                fontSize: 17,
                fontWeight: 'bold',
              }}>
              {/* {hum} */}
            </Text>
          </TouchableOpacity>
        </View>

        <View
          style={{
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            style={{
              marginLeft: 60,
              height: 100,
              width: 100,
            }}
            onPress={() => {
              AsyncStorage.getItem('accessToken').then(token => {
                dispatch(
                  fetchIdDevicesData({
                    id: devices[2].id,
                    token: token,
                  }),
                );
              });
              setDeviceId(devices[2].id);
              setModalDeleteDevice(true);
            }}>
            <Image
              source={icons.icon_humidity}
              style={[
                styles.icon_home,
                {
                  width: 56,
                  height: 56,
                  tintColor: devices[2].deviceType === 3 ? '#1E90FF' : 'grey',
                },
              ]}></Image>
            <Text
              style={{
                marginTop: 5,
                fontSize: 17,
                fontWeight: 'bold',
              }}>
              {/* {noice} */}
            </Text>
          </TouchableOpacity>
          <View style={{flex: 1}} />
          <TouchableOpacity
            style={{
              marginRight: 20,
              height: 100,
              width: 100,
            }}
            onPress={() => {
              console.log('Press noise');
              AsyncStorage.getItem('accessToken').then(token => {
                dispatch(
                  fetchIdDevicesData({
                    id: devices[3].id,
                    token: token,
                  }),
                );
              });
              setDeviceId(devices[3].id);
              setModalDeleteDevice(true);
            }}>
            <Image
              source={icons.icon_noise}
              style={[
                styles.icon_home,
                {
                  width: 56,
                  height: 56,
                  tintColor: devices[3].deviceType === 4 ? '#1E90FF' : 'grey',
                },
              ]}></Image>
            <Text
              style={{
                marginTop: 5,
                marginLeft: 22,
                fontSize: 17,
                fontWeight: 'bold',
              }}>
              {/* {angle} */}
            </Text>
          </TouchableOpacity>
        </View>

        <View
          style={{
            marginLeft: 10,
            marginVertical: 20,
            flexDirection: 'row',
            height: 50,
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image source={icons.icon_fan} style={styles.icon_fan_music}></Image>
          <Text
            style={{
              marginLeft: 5,
              fontSize: 16,
              fontWeight: 'bold',
            }}>
            Open fan
          </Text>
          <View style={{flex: 1}}></View>
          <Switch
            trackColor={{false: '#767577', true: '#99CCCC'}}
            thumbColor={isEnabledFan ? 'blue' : '#f4f3f4'}
            onValueChange={handleChangeFan}
            value={isEnabledFan}
            style={{
              marginRight: 20,
            }}
          />
        </View>
        <View
          style={{
            marginLeft: 10,
            flexDirection: 'row',
            height: 50,
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            source={icons.icon_music}
            style={styles.icon_fan_music}></Image>
          <Text
            style={{
              marginLeft: 5,
              fontSize: 16,
              fontWeight: 'bold',
            }}>
            Open music
          </Text>
          <View style={{flex: 1}}></View>
          <Switch
            trackColor={{false: '#767577', true: '#99CCCC'}}
            thumbColor={isEnabledMusic ? 'blue' : '#f4f3f4'}
            onValueChange={handleChangeMusic}
            value={isEnabledMusic}
            style={{
              marginRight: 20,
            }}
          />
        </View>

        <View style={styles.centeredView}>
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalPutCradle}
            onRequestClose={() => {
              setModalPutCradle(!modalPutCradle);
            }}>
            <View style={styles.centeredView}>
              <View style={[styles.modalView, {width: 280, height: 300}]}>
                <UITextModalDetail input="Name:" />
                <TextInput
                  showSoftInputOnFocus={false}
                  onChangeText={text => setName(text)}
                  defaultValue={Name}
                  style={styles.textInputModal}
                  editable={true}
                />
                <UITextModalDetail input="Description:" />
                <TextInput
                  showSoftInputOnFocus={false}
                  onChangeText={text => setDescription(text)}
                  defaultValue={Description}
                  style={styles.textInputModal}
                  editable={true}></TextInput>
                <UITextModalDetail input="Status:" />
                <TextInput
                  showSoftInputOnFocus={false}
                  onChangeText={text => setStatus(text)}
                  defaultValue={Status}
                  style={styles.textInputModal}
                  editable={true}></TextInput>

                <Pressable
                  style={[styles.button, styles.buttonClose]}
                  onPress={() => {
                    AsyncStorage.getItem('accessToken').then(token => {
                      dispatch(
                        fetchPutIdCradleData({
                          id: id,
                          data: {
                            name: Name,
                            description: Description,
                            status: Status,
                            avatar: 'string',
                          },
                          token: token,
                        }),
                      );
                    });
                    setModalPutCradle(!modalPutCradle);
                  }}>
                  <Text style={styles.textStyle}>Save</Text>
                </Pressable>
                <Pressable
                  style={[styles.button, styles.buttonClose]}
                  onPress={() => setModalPutCradle(!modalPutCradle)}>
                  <Text style={styles.textStyle}>Close</Text>
                </Pressable>
              </View>
            </View>
          </Modal>
        </View>
        <View style={styles.centeredView}>
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalDeleteCradle}
            onRequestClose={() => {
              setModalDeleteCradle(!modalDeleteCradle);
            }}>
            <View style={styles.centeredView}>
              <View style={[styles.modalView, {width: 200, height: 136}]}>
                <Text
                  style={{
                    // fontWeight: 'bold',
                    fontSize: 18,
                    color: 'black',
                    marginLeft: 10,
                    marginTop: 8,
                  }}>
                  Are you sure to delete {name}?
                </Text>
                <View style={{flexDirection: 'row'}}>
                  <Pressable
                    style={[
                      styles.btnModalDelete,
                      {backgroundColor: '#2196F3'},
                    ]}
                    onPress={() => {
                      AsyncStorage.getItem('accessToken').then(token => {
                        dispatch(
                          fetchDeleteIdCradleData({
                            id: id,
                            token: token,
                          }),
                        );
                      });
                      setModalDeleteCradle(!modalDeleteCradle);
                    }}>
                    <Text style={styles.textStyle}>Delete</Text>
                  </Pressable>
                  <Pressable
                    style={[styles.btnModalDelete, {backgroundColor: 'gray'}]}
                    onPress={() => setModalDeleteCradle(!modalDeleteCradle)}>
                    <Text style={styles.textStyle}>Close</Text>
                  </Pressable>
                </View>
              </View>
            </View>
          </Modal>
        </View>

        <View style={styles.centeredView}>
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalDeleteDevice}
            onRequestClose={() => {
              setModalDeleteDevice(!modalDeleteDevice);
            }}>
            <View style={styles.centeredView}>
              <View style={[styles.modalView, {width: 280, height: 340}]}>
                <UITextModalDetail input="Name:" />
                <TextInput
                  showSoftInputOnFocus={false}
                  onChangeText={text => setDeviceName(text)}
                  defaultValue={DeviceData.name}
                  style={styles.textInputModal}
                  editable={true}
                />

                <UITextModalDetail input="Descripton:" />
                <TextInput
                  showSoftInputOnFocus={false}
                  onChangeText={text => setDeviceDes(text)}
                  defaultValue={DeviceData.description}
                  style={styles.textInputModal}
                  editable={true}></TextInput>

                <UITextModalDetail input="Avatar:" />
                <TextInput
                  showSoftInputOnFocus={false}
                  onChangeText={text => setAvatar(text)}
                  defaultValue={DeviceData.avatar}
                  style={styles.textInputModal}
                  editable={true}></TextInput>
                <Pressable
                  style={[styles.button, styles.buttonClose]}
                  onPress={() => {
                    console.log('Press temp save');
                    console.log(DeviceName ? DeviceName : DeviceData.name);
                    console.log(Avatar ? Avatar : DeviceData.avatar);

                    AsyncStorage.getItem('accessToken').then(token => {
                      dispatch(
                        fetchPutIdDevice({
                          id: DeviceId,
                          token: token,
                          data: {
                            name: DeviceName ? DeviceName : DeviceData.name,
                            description: DeviceDes
                              ? DeviceDes
                              : DeviceData.description,
                            avatar: Avatar ? Avatar : DeviceData.avatar,
                          },
                        }),
                      );
                    });
                    setDeviceId('');
                    setDeviceName('');
                    setDeviceDes('');
                    setModalDeleteDevice(!modalDeleteDevice);
                  }}>
                  <Text style={styles.textStyle}>Save</Text>
                </Pressable>
                <Pressable
                  style={[styles.button, styles.buttonClose]}
                  onPress={() => {
                    console.log('Press temp delete');
                    AsyncStorage.getItem('accessToken').then(token => {
                      dispatch(
                        fetchDeleteIdDevice({
                          id: DeviceId,
                          token: token,
                        }),
                      );
                    });
                    setDeviceId('');
                    setDeviceName('');
                    setDeviceDes('');
                    setModalDeleteDevice(!modalDeleteDevice);
                  }}>
                  <Text style={styles.textStyle}>Delete</Text>
                </Pressable>
                <Pressable
                  style={[styles.button, styles.buttonClose]}
                  onPress={() => {
                    setDeviceId('');
                    setDeviceName('');
                    setDeviceDes('');
                    setModalDeleteDevice(!modalDeleteDevice);
                  }}>
                  <Text style={styles.textStyle}>Close</Text>
                </Pressable>
              </View>
            </View>
          </Modal>
        </View>
      </View>
    )
  );
}
const styles = StyleSheet.create({
  icon_home: {
    marginHorizontal: 5,
  },
  icon_fan_music: {
    marginHorizontal: 5,
    width: 30,
    height: 30,
    tintColor: '#1E90FF',
  },
  textChangeName: {
    marginVertical: 30,
    fontSize: 25,
    fontWeight: 'bold',
    marginLeft: 10,
  },
  textInputChange: {
    height: 70,
    borderColor: 'gray',
    borderWidth: 1,
    fontSize: 25,
    backgroundColor: 'red',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    marginLeft: 13,
  },
  modalView: {
    marginRight: 8,
    marginBottom: 20,
    backgroundColor: '#EEEEEE',
    borderRadius: 20,
    borderWidth: 2,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    marginHorizontal: 40,
    marginTop: 5,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  btnModalDelete: {
    borderRadius: 12,
    padding: 8,
    marginHorizontal: 20,
    marginTop: 15,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    textAlign: 'center',
  },

  textInputModal: {
    marginTop: 5,
    color: 'black',
    marginLeft: 10,
    borderColor: 'white',
    height: 40,
    paddingLeft: 18,
    marginHorizontal: 5,
    backgroundColor: 'white',
    fontSize: 17,
    borderRadius: 30,
  },
});

export default DetailCradle;
