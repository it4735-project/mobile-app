import {Text} from 'react-native';
import {React} from 'react-native';

function UITextModalHome({input}) {
  return (
    <Text
      style={{
        marginTop: 10,
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 10,
      }}>
      {input}
    </Text>
  );
}

export default UITextModalHome;
