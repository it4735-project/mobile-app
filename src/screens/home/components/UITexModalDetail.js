import {Text} from 'react-native';
import {React} from 'react-native';

function UITextModalDetail({input}) {
  return (
    <Text
      style={{
        fontSize: 16,
        fontWeight: 'bold',
        marginLeft: 20,
        color: 'black',
      }}>
      {input}
    </Text>
  );
}

export default UITextModalDetail;
