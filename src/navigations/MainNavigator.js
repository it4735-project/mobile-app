import React from 'react';

import AsyncStorage from '@react-native-async-storage/async-storage';
import AuthNavigator from './AuthNavigator';
import HomeNavigator from './HomeNavigator';
import {NavigationContainer} from '@react-navigation/native';
import {useDispatch} from 'react-redux';
import {useCheckAuth} from '../hooks/useCheckAuth';
import {fetchUserData} from '../features/auth/auth.slice';
import {useGetCradle} from '../hooks/useGetCradle';

const MainNavigator = () => {
  const dispatch = useDispatch();
  const {isLoggedIn, currentUser} = useCheckAuth();
  const [DataCradle] = useGetCradle();
  const onStateChange = () => {
    (!isLoggedIn || !currentUser) &&
      AsyncStorage.getItem('accessToken').then(token => {
        dispatch(fetchUserData(token));
      });
  };

  return (
    <>
      <NavigationContainer onStateChange={onStateChange}>
        {isLoggedIn ? <HomeNavigator /> : <AuthNavigator />}
      </NavigationContainer>
    </>
  );
};

export default MainNavigator;
