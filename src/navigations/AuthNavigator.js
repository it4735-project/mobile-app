import React from 'react';

import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Register} from '../screens/register';
import {LoginScreen} from '../screens/login';
const AuthStack = createNativeStackNavigator();

const AuthNavigator = () => {
  return (
    <AuthStack.Navigator screenOptions={{headerShown: false}}>
      <AuthStack.Screen name="Login" component={LoginScreen} />
      <AuthStack.Screen name="Register" component={Register} />
    </AuthStack.Navigator>
  );
};

export default AuthNavigator;
