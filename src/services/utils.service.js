import axios from 'axios';

import {axiosMethod} from '../constants/axiosMethod';

export const axiosRequest = async (
  url,
  method = axiosMethod.GET,
  token = '',
  params,
  data = null,
) => {
  const response = await axios({
    url,
    method,
    params,
    headers: {
      Authorization: `Bearer ${token}`,
    },
    data,
  }).catch(err => {
    console.log(err);
  });
  // console.log('=============== res from services ', response.data);
  return response.data;
};
