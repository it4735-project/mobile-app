import {useSelector} from 'react-redux';

export const useCheckAuth = () => {
  const isLoggedIn = useSelector(state => state.auth.isLoggedIn);
  const currentUser = useSelector(state => state.auth.currentUser);

  return {isLoggedIn, currentUser};
};
