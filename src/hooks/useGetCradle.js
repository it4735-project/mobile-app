import {useSelector} from 'react-redux';

export const useGetCradle = () => {
  const CradleData = useSelector(state => state.cradle.currentCradleData);
  const IdCradle = useSelector(state => state.cradle.IdCradle);
  return [CradleData, IdCradle];
};
export const useGetIdCradle = () => {
  const IdCradle = useSelector(state => state.cradle.IdCradle);
  return [IdCradle];
};
export const useGetIdCradleData = () => {
  const IdCradleData = useSelector(state => state.cradle.currentIdCradleData);
  return IdCradleData;
};

export const useGetDeviceIds = () => {
  const deviceIds = useSelector(state => state.cradle.currentDeviceIds);
  return [deviceIds];
};
