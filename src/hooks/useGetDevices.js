import {useSelector} from 'react-redux';

export const useGetDevices = () => {
  const DevicesData = useSelector(state => state.devices.AllDevices);
  //console.log('Devicesdata', DevicesData);
  return [DevicesData];
};

export const useGetIdDevice = () => {
  const DeviceData = useSelector(state => state.devices.IdDevices);
  return DeviceData;
};
