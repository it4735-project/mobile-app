import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';
import AsyncStorage from '@react-native-async-storage/async-storage';
const initialState = {
  accessToken: '',
  currentUser: null,
  isAuthenticated: false,
  isPendingGetMe: false,
  isPendingLogin: false,
  isPendingRegister: false,
  isLoggedIn: false,
  errorMessage: '',
};

import authApi from './auth.api';

export const fetchSignIn = createAsyncThunk(
  'auth/login',
  async (data, {rejectWithValue}) => {
    try {
      const response = await authApi.login(data);
      await AsyncStorage.removeItem('accessToken');
      await AsyncStorage.setItem('accessToken', response.token);
      return response;
    } catch (error) {
      console.log(error);
      return rejectWithValue(error.response.data);
    }
  },
);

export const fetchUserData = createAsyncThunk(
  'auth/getMe',
  async (token, {rejectWithValue}) => {
    try {
      const response = await authApi.getMe(token);
      console.log('res from get ME', response);
      return response;
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  },
);

export const fetchSignUp = createAsyncThunk(
  'auth/register',
  async (data, {rejectWithValue}) => {
    try {
      const response = await authApi.register(data);
      await AsyncStorage.removeItem('accessToken');
      await AsyncStorage.setItem('accessToken', response.token);
      return response;
    } catch (error) {
      console.log(error);
      return rejectWithValue(error.response.data);
    }
  },
);

const authSlice = createSlice({
  name: 'auth',
  initialState,

  reducers: {
    login(state, action) {
      state.isLoggedIn = true;
      state.currentUser = action.payload.userData;
    },
    logout(state, action) {
      state.isLoggedIn = false;
      state.currentUser = null;
      state.accessToken = '';
    },
  },

  extraReducers: builder => {
    builder
      .addCase(fetchSignIn.pending, (state, action) => {
        state.isPendingLogin = true;
        state.isLoggedIn = false;
      })
      .addCase(fetchSignIn.fulfilled, (state, action) => {
        state.isPendingLogin = false;
        state.isLoggedIn = true;
        state.accessToken = action.payload.token;
      })
      .addCase(fetchSignIn.rejected, (state, action) => {
        state.isPendingLogin = false;
        state.isLoggedIn = false;
        state.errorMessage = action.payload;
      })
      .addCase(fetchUserData.pending, (state, action) => {
        state.isPendingGetMe = true;
      })
      .addCase(fetchUserData.fulfilled, (state, action) => {
        state.isPendingGetMe = false;
        state.currentUser = action.payload;
        state.isLoggedIn = true;
      })
      .addCase(fetchUserData.rejected, (state, action) => {
        state.isPendingGetMe = false;
        state.errorMessage = action.payload;
        state.isLoggedIn = false;
      })
      .addCase(fetchSignUp.pending, (state, action) => {
        state.isPendingRegister = true;
      })
      .addCase(fetchSignUp.fulfilled, (state, action) => {
        state.isPendingRegister = false;
        state.accessToken = action.payload.token;
      })
      .addCase(fetchSignUp.rejected, (state, action) => {
        state.isPendingRegister = false;
        state.errorMessage = action.payload;
      });
  },
});

export const {login, logout} = authSlice.actions;
export default authSlice.reducer;
