import {axiosRequest} from '../../services/utils.service';
import {axiosMethod} from '../../constants/axiosMethod';
import {API_END_POINT} from '@env';

class AuthenApi {
  constructor() {
    this.apiEndPoint = API_END_POINT;
  }

  login = async data => {
    const url = `${this.apiEndPoint}/api/auth/login`;
    return await axiosRequest(url, axiosMethod.POST, null, null, data);
  };

  register = async (data, token) => {
    const url = `${this.apiEndPoint}/api/auth/register`;
    return await axiosRequest(url, axiosMethod.POST, token, null, data);
  };

  getMe = async token => {
    const url = `${this.apiEndPoint}/api/user/getMe`;
    console.log('url get ME', url);
    return await axiosRequest(url, axiosMethod.GET, token, null, null);
  };
}

export default new AuthenApi();
