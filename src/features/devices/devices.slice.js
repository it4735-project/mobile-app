import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';
import devicesApi from './devices.api';
const initialState = {
  accessToken: '',
  AllDevices: [],
  errorMessage: '',
  isPendingGetAllDevices: null,
  isPendingGetIdDevice: null,
  isPendingPostDevice: null,
  isPendingPutIdDevice: null,
  isPendingDeleteIdDevice: null,
  IdDevices: {},
};

export const fetchAllDevicesData = createAsyncThunk(
  'devices/getAllDevices',
  async (token, {rejectWithValue}) => {
    try {
      const response = await devicesApi.getAllDevices(token);

      return response;
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  },
);
export const fetchIdDevicesData = createAsyncThunk(
  'devices/getIdDevice',
  async ({id, token}, {rejectWithValue}) => {
    try {
      const response = await devicesApi.getIdDevices(id, token);
      console.log('======= res from GET ID Device', response);
      return response;
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  },
);

export const fetchPutIdDevice = createAsyncThunk(
  'devices/PutIdDevice',
  async ({id, token, data}, {rejectWithValue}) => {
    try {
      const response = await devicesApi.putIdDevice(id, token, data);
      console.log('=========== res PUT API Device ', response);
      return response;
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  },
);
export const fetchDeleteIdDevice = createAsyncThunk(
  'devices/DeleteIdDevice',
  async ({id, token}, {rejectWithValue}) => {
    try {
      const response = await devicesApi.deleteIdDevice(id, token);
      console.log('=========== res DELETE API Device ', response);
      return response;
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  },
);

const devicesSlice = createSlice({
  name: 'devices',
  initialState,

  reducers: {},

  extraReducers: builder => {
    builder
      .addCase(fetchAllDevicesData.pending, (state, action) => {
        state.isPendingGetAllDevices = true;
      })
      .addCase(fetchAllDevicesData.fulfilled, (state, action) => {
        state.isPendingGetAllDevices = false;
        state.AllDevices = action.payload.data;
      })
      .addCase(fetchAllDevicesData.rejected, (state, action) => {
        state.isPendingGetAllDevices = false;
        state.errorMessage = action.payload;
      })

      .addCase(fetchIdDevicesData.pending, (state, action) => {
        state.isPendingGetIdDevice = true;
      })
      .addCase(fetchIdDevicesData.fulfilled, (state, action) => {
        state.isPendingGetIdDevice = false;
        state.IdDevices = action.payload.data;
      })
      .addCase(fetchIdDevicesData.rejected, (state, action) => {
        state.isPendingGetIdDevice = false;
        state.errorMessage = action.payload;
      })

      .addCase(fetchPutIdDevice.pending, (state, action) => {
        state.isPendingPutIdDevice = true;
      })
      .addCase(fetchPutIdDevice.fulfilled, (state, action) => {
        state.isPendingPutIdDevice = false;
      })
      .addCase(fetchPutIdDevice.rejected, (state, action) => {
        state.isPendingPutIdDevice = false;
        state.errorMessage = action.payload;
      })

      .addCase(fetchDeleteIdDevice.pending, (state, action) => {
        state.isPendingDeleteIdDevice = true;
      })
      .addCase(fetchDeleteIdDevice.fulfilled, (state, action) => {
        state.isPendingDeleteIdDevice = false;
      })
      .addCase(fetchDeleteIdDevice.rejected, (state, action) => {
        state.isPendingDeleteIdDevice = false;
        state.errorMessage = action.payload;
      });
  },
});
export default devicesSlice.reducer;
