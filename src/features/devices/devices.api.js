import {axiosRequest} from '../../services/utils.service';
import {axiosMethod} from '../../constants/axiosMethod';
import {API_END_POINT} from '@env';

class AuthenApi {
  constructor() {
    this.apiEndPoint = API_END_POINT;
  }

  getAllDevices = async token => {
    const url = `${this.apiEndPoint}/api/devices`;
    return await axiosRequest(url, axiosMethod.GET, token, null, null);
  };

  getIdDevices = async (id, token) => {
    const url = `${this.apiEndPoint}/api/devices/${id}`;
    return await axiosRequest(url, axiosMethod.GET, token, null, null);
  };

  postDevice = async (token, data) => {
    const url = `${this.apiEndPoint}/api/devices`;
    return await axiosRequest(url, axiosMethod.POST, token, null, data);
  };

  putIdDevice = async (id, token, data) => {
    const url = `${this.apiEndPoint}/api/devices/${id}`;
    return await axiosRequest(url, axiosMethod.PUT, token, null, data);
  };

  deleteIdDevice = async (id, token) => {
    const url = `${this.apiEndPoint}/api/devices/${id}`;
    return await axiosRequest(url, axiosMethod.DELETE, token, null, null);
  };
}

export default new AuthenApi();
