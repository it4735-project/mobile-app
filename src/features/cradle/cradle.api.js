import {axiosRequest} from '../../services/utils.service';
import {axiosMethod} from '../../constants/axiosMethod';
import {API_END_POINT} from '@env';

class cradleApi {
  constructor() {
    this.apiEndPoint = API_END_POINT;
  }

  getCradle = async token => {
    const url = `${this.apiEndPoint}/api/cradle/all`;
    return await axiosRequest(url, axiosMethod.GET, token, null, null);
  };

  // getIdCradle = async id => {
  //   const url = `${this.apiEndPoint}/api/cradle/${id}`;
  //   console.log('url get Id cradle', url);
  //   return await axiosRequest(url, axiosMethod.GET, null, null, null);
  // };

  deleteIdCradle = async (id, token) => {
    const url = `${this.apiEndPoint}/api/cradle/${id}`;
    console.log('url delete id cradle', url);
    return await axiosRequest(url, axiosMethod.DELETE, token, null, null);
  };

  putIdCradle = async (id, data, token) => {
    const url = `${this.apiEndPoint}/api/cradle/${id}`;
    console.log(' url put id cradle', url);
    return await axiosRequest(url, axiosMethod.PUT, token, null, data);
  };

  postCradle = async (data, token) => {
    const url = `${this.apiEndPoint}/api/cradle`;
    return await axiosRequest(url, axiosMethod.POST, token, null, data);
  };
}

export default new cradleApi();
