import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';
import cradleApi from './cradle.api';
const initialState = {
  accessToken: '',
  currentCradleData: [],
  currentIdCradleData: null,
  isPendingGetCradleData: false,
  IdCradle: [],
  errorMessage: '',
  currentDeviceIds: [],
  isPendingGetIdCradleData: null,
  isPendingDeleteIdCradleData: null,
  isPendingPutIdCradleData: null,
  isPendingPostCradleData: null,
  isPendingAddDevice: null,
  isPendingRemoveDevice: null,
};

export const fetchCradleData = createAsyncThunk(
  'cradle/getCradle',
  async (token, {rejectWithValue}) => {
    try {
      const response = await cradleApi.getCradle(token);
      console.log('=== res from get all cradle', response);
      return response;
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  },
);
// export const fetchIdCradleData = createAsyncThunk(
//   'cradle/getIdDataCradle',
//   async (id, {rejectWithValue}) => {
//     try {
//       const response = await cradleApi.getIdCradle(id);
//       console.log('======= res from data id cradle', response);
//       return response;
//     } catch (error) {
//       return rejectWithValue(error.response.data);
//     }
//   },
// );
export const fetchDeleteIdCradleData = createAsyncThunk(
  'cradle/deleteIdDataCradle',
  async ({id, token}, {rejectWithValue}) => {
    try {
      console.log('id token', id, token);
      const response = await cradleApi.deleteIdCradle(id, token);
      console.log('======== res from delete id cradle', response);
      return response;
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  },
);
export const fetchPutIdCradleData = createAsyncThunk(
  'cradle/putIdDataCradle',
  async ({id, data, token}, {rejectWithValue}) => {
    try {
      const response = await cradleApi.putIdCradle(id, data, token);
      console.log('========= res from put id cradle', response);
      return response;
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  },
);
export const fetchPostCradleData = createAsyncThunk(
  'cradle/PostDataCradle',
  async ({token, data}, {rejectWithValue}) => {
    try {
      const response = await cradleApi.postCradle(data, token);
      console.log('========== res from post cradle', response);
      return response;
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  },
);

// export const fetchAddDevice = createAsyncThunk(
//   'cradle/AddDevice',
//   async ({id, token, deviceId}, {rejectWithValue}) => {
//     try {
//       const response = await cradleApi.addDevices(id, token, deviceId);
//       console.log('=============== response from add Device cradle', response);
//       return response;
//     } catch (error) {
//       return rejectWithValue(error.response.data);
//     }
//   },
// );
// export const fetchRemoveDevice = createAsyncThunk(
//   'cradle/RemoveDevice',
//   async ({id, token, deviceId}, {rejectWithValue}) => {
//     try {
//       const response = await cradleApi.removeDevices(id, token, deviceId);
//       console.log(
//         '=============== response from remove Device cradle',
//         response,
//       );
//       return response;
//     } catch (error) {
//       return rejectWithValue(error.response.data);
//     }
//   },
// );
const cradleSlice = createSlice({
  name: 'cradle',
  initialState,

  reducers: {},

  extraReducers: builder => {
    builder

      .addCase(fetchCradleData.pending, (state, action) => {
        state.isPendingGetCradleData = true;
      })
      .addCase(fetchCradleData.fulfilled, (state, action) => {
        state.isPendingGetCradleData = false;
        state.currentCradleData = action.payload;
        // action.payload.map(item => {
        //   state.IdCradle.push(item.id);
        // });
      })
      .addCase(fetchCradleData.rejected, (state, action) => {
        state.isPendingGetCradleData = false;
        state.errorMessage = action.payload;
      })

      // .addCase(fetchIdCradleData.pending, (state, action) => {
      //   state.isPendingGetIdCradleData = true;
      // })
      // .addCase(fetchIdCradleData.fulfilled, (state, action) => {
      //   state.isPendingGetIdCradleData = false;
      //   state.currentIdCradleData = action.payload;
      //   state.currentDeviceIds = action.payload.deviceIds;
      //   console.log('======== deviceIds', state.currentDeviceIds);
      // })
      // .addCase(fetchIdCradleData.rejected, (state, action) => {
      //   state.isPendingGetIdCradleData = false;
      //   state.errorMessage = action.payload;
      // })

      .addCase(fetchDeleteIdCradleData.pending, (state, action) => {
        state.isPendingDeleteIdCradleData = true;
      })
      .addCase(fetchDeleteIdCradleData.fulfilled, (state, action) => {
        state.isPendingDeleteIdCradleData = false;
      })
      .addCase(fetchDeleteIdCradleData.rejected, (state, action) => {
        state.isPendingDeleteIdCradleData = false;
        state.errorMessage = action.payload;
      })

      // .addCase(fetchPutIdCradleData.pending, (state, action) => {
      //   state.isPendingPutIdCradleData = true;
      // })
      // .addCase(fetchPutIdCradleData.fulfilled, (state, action) => {
      //   state.isPendingPutIdCradleData = false;
      // })
      // .addCase(fetchPutIdCradleData.rejected, (state, action) => {
      //   state.isPendingPutIdCradleData = false;
      //   state.errorMessage = action.payload;
      // })

      .addCase(fetchPostCradleData.pending, (state, action) => {
        state.isPendingPostCradleData = true;
      })
      .addCase(fetchPostCradleData.fulfilled, (state, action) => {
        state.isPendingPostCradleData = false;
        console.log('res from post cradle', action.payload);
      })
      .addCase(fetchPostCradleData.rejected, (state, action) => {
        state.isPendingPostCradleData = false;
        state.errorMessage = action.payload;
      });

    // .addCase(fetchAddDevice.pending, (state, action) => {
    //   state.isPendingAddDevice = true;
    // })
    // .addCase(fetchAddDevice.fulfilled, (state, action) => {
    //   state.isPendingAddDevice = false;
    // })
    // .addCase(fetchAddDevice.rejected, (state, action) => {
    //   state.isPendingAddDevice = false;
    //   state.errorMessage = action.payload;
    // })

    // .addCase(fetchRemoveDevice.pending, (state, action) => {
    //   state.isPendingRemoveDevice = true;
    // })
    // .addCase(fetchRemoveDevice.fulfilled, (state, action) => {
    //   state.isPendingRemoveDevice = false;
    // })
    // .addCase(fetchRemoveDevice.rejected, (state, action) => {
    //   state.isPendingRemoveDevice = false;
    //   state.errorMessage = action.payload;
    // });
  },
});
export default cradleSlice.reducer;
