import {React} from 'react-native';
import 'react-native-gesture-handler';
import {Provider} from 'react-redux';
import {store} from './src/redux/store';
import MainNavigator from './src/navigations/MainNavigator';
import {HubConnectionBuilder, LogLevel} from '@microsoft/signalr';

const App = () => {
  const connection = new HubConnectionBuilder()
    .withUrl('http://103.130.214.71:5024/hub')
    .configureLogging(LogLevel.Information)
    .build();
  connection.on('SendData', message => {
    console.log('Message received', message);
  });
  connection.start();
  return (
    <Provider store={store}>
      <MainNavigator />
    </Provider>
  );
};

export default App;
